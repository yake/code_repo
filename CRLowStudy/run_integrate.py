import ROOT

# nameTemplate is e.g. Wbc_2bbtag2jet_75_150ptv_CRLowT0_mva
# And you want to replace it into:
# Wbc_2bbtag2jet_75_150ptv_CRLowT1_mva
# CRLowStr is "CRLowT1" 
def get_replaced_h_name(nameTemplate, CRLowStr):
    # for example: splitted = ['Wbb', '2bbtag2jet', '75', '150ptv', 'CRLowT3', 'pTV']
    splitted = nameTemplate.split("_")
    for idx, s in enumerate(splitted):
        if "CRLow" in s:
            if "mvaCRLow" == s:
                print("There shouldn't be mvaCRLow! Check it!")
                exit()
            else:
                splitted[idx] = CRLowStr
                break
    return "_".join(splitted)    

# Return which CRLow region(e.g. CRLowT1), judge by histogram name
def get_CRLow_region_name(name):
    splitted = name.split("_")
    CRLow_region = ""
    for idx, s in enumerate(splitted):
        if "mvaCRLow" == s:
            print("There shouldn't be mvaCRLow! Check it!")
            exit()
        elif "CRLow" in s:
            CRLow_region = s
            break
    return CRLow_region

# Return which CRLow regions should be kept/discarded
def get_CRLow_region_lst(mva_score_low_bound):
    CRLow_full_lst = ["CRLowF0",
            "CRLowT0",
            "CRLowT1",
            "CRLowT2",
            "CRLowT3",
            "CRLowT4",
            "CRLowT5",
            "CRLowT6",
            "CRLowT7",
            "CRLowT8",
            "CRLowT9"]
    lst_idx = int(round(mva_score_low_bound*10)+1)
    if (lst_idx < 0):
        lst_idx = 0
    if (lst_idx > 10):
        print("Error! The mva score lower bound has to be <= 0.9 and a multiple of 0.1")
        print("Also, e.g. -0.2 is legal.")
        exit()
    CRLow_retain_lst = CRLow_full_lst[lst_idx:]
    CRLow_discard_lst = CRLow_full_lst[:lst_idx]

    return CRLow_retain_lst, CRLow_discard_lst
    
def integrate_hists(CRLow_obj_lst, CRLow_retain_lst, CRLow_discard_lst):
    h_dict = {}
    h_name_lst = []
    integrated_h_lst = []
    for hist in CRLow_obj_lst:
        h_name = hist.GetName()
        # For not, ignore mvaCRLow histograms
        if "mvaCRLow" in h_name:
            continue
        # simply ignore all hists in CRLow_discard_lst
        if get_CRLow_region_name(h_name) in CRLow_discard_lst:
            continue
        h_dict[h_name] = hist
        h_name_lst.append(h_name)
    
    for h_name in h_name_lst:
        if h_name in h_dict:
            hist = h_dict[h_name]
            #print("integrating: {}, with integral:{}".format(h_name, hist.Integral()))
            del h_dict[h_name]
            integrated_name = get_replaced_h_name(h_name, "CRLow")
            integrated_hist = hist.Clone(integrated_name)
            integrated_hist.AddDirectory(ROOT.kFALSE)
            integrated_hist.SetTitle(integrated_name)
            integrated_hist.SetName(integrated_name)
            # Add all histograms the same as the current one but in
            # different retain CRLow region
            for CRLow_retain_region in CRLow_retain_lst:
                tmp_name = get_replaced_h_name(h_name, CRLow_retain_region)
                if tmp_name in h_dict:
                    integrated_hist.Add(h_dict[tmp_name])
                    #print("adding {}, integral:{}".format(tmp_name, integrated_hist.Integral()))
                    del h_dict[tmp_name]
            integrated_h_lst.append(integrated_hist)
            
    return integrated_h_lst

def get_new_path(old_path, dir_name):
    if not old_path:
        return dir_name
    else:
        return old_path+"/"+dir_name


def recursive_integrate_directory(relative_path, fin_name, fout, CRLow_retain_lst, CRLow_discard_lst):
    #######################
    fin = ROOT.TFile(fin_name,"READ")
    if (relative_path == ""):
        loc_out = fout
        loc_in = fin
    else:
        loc_out = fout.Get(relative_path)
        loc_in = fin.Get(relative_path)
    cur_path = loc_out.GetPath().split(":")[1][1:]
    #######################
    print("Integrating directory: {}".format(loc_in))
    obj_key_lst = loc_in.GetListOfKeys()
    CRLow_obj_lst = []
    other_obj_lst = []
    #dir_cnt = 0 # debug purpose, delete once done
    for key in obj_key_lst:
        if key.GetClassName() == "TDirectoryFile":
            #if dir_cnt > 2: # debug purpose, delete once done
            #    continue    # debug purpose, delete once done
            #dir_cnt += 1 # debug purpose, delete once done
            loc_out.mkdir(key.GetName())
            next_loc_out = get_new_path(cur_path, key.GetName())
            next_loc_in = loc_in.Get(key.GetName())
            recursive_integrate_directory(next_loc_out, fin_name, fout, CRLow_retain_lst, CRLow_discard_lst)
        else:
            #obj = loc_in.Get(key.GetName()) # Very slow, ~20 seconds for each directory
            obj = key.ReadObj()
            # AddDirectory(ROOT.kFalse) prevent ROOT book keeping histogram to make function return faster 
            # https://root-forum.cern.ch/t/closing-root-files-is-dead-slow-is-this-a-normal-thing/5273/8
            if key.GetClassName() == "TH1F":
                obj.AddDirectory(ROOT.kFALSE)
            if "CRLow" in key.GetName():
                CRLow_obj_lst.append(obj)
            else:
                other_obj_lst.append(obj)
    
    # Writing out none CRLow, none tdirectory objects
    fout.cd(loc_out.GetPath())
    for obj in other_obj_lst:
        obj.Write()
    
    integrated_h_lst = integrate_hists(CRLow_obj_lst, CRLow_retain_lst, CRLow_discard_lst)
    # Writing out CRLow histograms
    for hist in integrated_h_lst:
        hist.Write()
    
    fin.Close()
    
# Performing integration on the whole file
#mvaScoreLowBound = 0.6 # Should be positive multiple of 0.1 or negative.
def integrate_file(mva_score_low_bound, idx, fin_name, fout_name):    
    # Preparing i/o files
    fout_name += str(idx)+".root"
    fin = ROOT.TFile(fin_name, "READ")
    fout = ROOT.TFile(fout_name, "RECREATE")

    # Fetch list of CRLow histograms to retain/discard
    CRLow_retain_lst, CRLow_discard_lst = get_CRLow_region_lst(mva_score_low_bound)

    # Copy and integrate CRLow histograms recursively
    recursive_integrate_directory("", fin_name, fout, \
                                  CRLow_retain_lst, CRLow_discard_lst)

def main():
    #fin_name = "hist-ttbar_nonallhad_PwPy8.root"
    #fin_name = "fineSplit.root"
    fin_name = "/afs/cern.ch/user/y/yake/eos/yan/CxAOD/CRLow_ntuple/fineSplitGrid_validCDI_syst/fineSplitGrid_validCDI_syst_submission/fineSplitGrid_validCDI.root"
    fout_name = "fineSplitIntegrated" # prefix of output file name
    #mvaScoreLst = [0.1*i for i in range(0, 10)]
    mvaScoreLst = [0.1*i for i in range(5, 6)]
    for mvaScoreLowBound in mvaScoreLst:
        print("Start Integrating mvaScoreLowBound = "+str(mvaScoreLowBound))
        integrate_file(mvaScoreLowBound, int(round(mvaScoreLowBound*10)), fin_name, fout_name)
        print("mvaScoreLowBound = "+str(mvaScoreLowBound)+" integration is done.")

if __name__ == "__main__":
    main()
