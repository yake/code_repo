Analysis(Run2MCcalib,bottom,DL1r,FixedCutBEff_60,AntiKtVR30Rmax4Rmin02TrackJets){
	bin(250.0<pt<400.0,0<abseta<2.5)
	{
		central_value(1.00,0.002)
		sys(TRK_RES_Z0_MEAS,0.17%)
		sys(TRK_RES_D0_MEAS,0.07%)
	}
	bin(400.0<pt<500.0,0<abseta<2.5)
	{
		central_value(1.00,0.004)
		sys(TRK_RES_Z0_MEAS,0.07%)
		sys(TRK_RES_D0_MEAS,0.14%)
	}
	bin(500.0<pt<700.0,0<abseta<2.5)
	{
		central_value(1.00,0.004)
		sys(TRK_RES_Z0_MEAS,0.34%)
		sys(TRK_RES_D0_MEAS,-0.09%)
	}
	bin(700.0<pt<900.0,0<abseta<2.5)
	{
		central_value(1.00,0.006)
		sys(TRK_RES_Z0_MEAS,0.50%)
		sys(TRK_RES_D0_MEAS,0.08%)
	}
	bin(900.0<pt<1100.0,0<abseta<2.5)
	{
		central_value(1.00,0.010)
		sys(TRK_RES_Z0_MEAS,0.57%)
		sys(TRK_RES_D0_MEAS,0.68%)
	}
	bin(1100.0<pt<1400.0,0<abseta<2.5)
	{
		central_value(1.00,0.011)
		sys(TRK_RES_Z0_MEAS,0.62%)
		sys(TRK_RES_D0_MEAS,0.26%)
	}
	bin(1400.0<pt<1700.0,0<abseta<2.5)
	{
		central_value(1.00,0.017)
		sys(TRK_RES_Z0_MEAS,1.20%)
		sys(TRK_RES_D0_MEAS,1.05%)
	}
	bin(1700.0<pt<2000.0,0<abseta<2.5)
	{
		central_value(1.00,0.022)
		sys(TRK_RES_Z0_MEAS,0.90%)
		sys(TRK_RES_D0_MEAS,1.02%)
	}
	bin(2000.0<pt<2500.0,0<abseta<2.5)
	{
		central_value(1.00,0.025)
		sys(TRK_RES_Z0_MEAS,1.03%)
		sys(TRK_RES_D0_MEAS,0.90%)
	}
	bin(2500.0<pt<3000.0,0<abseta<2.5)
	{
		central_value(1.00,0.032)
		sys(TRK_RES_Z0_MEAS,1.07%)
		sys(TRK_RES_D0_MEAS,0.72%)
	}
}
