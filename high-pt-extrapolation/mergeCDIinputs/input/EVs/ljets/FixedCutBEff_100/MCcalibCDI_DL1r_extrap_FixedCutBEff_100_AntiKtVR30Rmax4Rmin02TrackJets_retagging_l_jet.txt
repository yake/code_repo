Analysis(Run2MCcalib,light,DL1r,FixedCutBEff_100,AntiKtVR30Rmax4Rmin02TrackJets){
	bin(300<pt<500,0<abseta<2.5)
	{
		central_value(1,00,0)
		sys(l_Eigen_2,0.35%)
		sys(l_Eigen_0,-0.08%)
		sys(l_Eigen_1,0.16%)
	}

	bin(500<pt<800,0<abseta<2.5)
	{
		central_value(1,00,0)
		sys(l_Eigen_2,0.71%)
		sys(l_Eigen_0,-0.08%)
		sys(l_Eigen_1,0.33%)
	}

	bin(800<pt<1200,0<abseta<2.5)
	{
		central_value(1,00,0)
		sys(l_Eigen_2,0.86%)
		sys(l_Eigen_0,0.05%)
		sys(l_Eigen_1,0.31%)
	}

}