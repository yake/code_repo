Analysis(Run2MCcalib,light,DL1r,FixedCutBEff_85,AntiKtVR30Rmax4Rmin02TrackJets){
	bin(300<pt<500,0<abseta<2.5)
	{
		central_value(1,00,0)
		sys(l_Eigen_2,-0.50%)
		sys(l_Eigen_0,-5.44%)
		sys(l_Eigen_1,2.70%)
	}

	bin(500<pt<800,0<abseta<2.5)
	{
		central_value(1,00,0)
		sys(l_Eigen_2,-1.06%)
		sys(l_Eigen_0,-9.65%)
		sys(l_Eigen_1,3.63%)
	}

	bin(800<pt<1200,0<abseta<2.5)
	{
		central_value(1,00,0)
		sys(l_Eigen_2,-1.83%)
		sys(l_Eigen_0,-12.54%)
		sys(l_Eigen_1,2.15%)
	}

}