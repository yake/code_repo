import ROOT

f_PCFT = ROOT.TFile("DL1r_TUNC_NBLS_rc_Zprime_AntiKtVR30Rmax4Rmin02TrackJets_b_jet_PCFT.root","READ")
f_std  = ROOT.TFile("DL1r_TUNC_NBLS_rc_Zprime_AntiKtVR30Rmax4Rmin02TrackJets_b_jet.root","READ")

folder_path = "AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903/TRK_RES_D0_DEAD/nominal/"

bs_id = 195
hist_name_pre = "taggedb_FixedCutBEff_"
hist_name_post = "|nominal_bootstrap_"
hist_name_truth = "truthb_nominal_bootstrap_"+str(bs_id)
hist_name_100 = hist_name_pre+"100"+hist_name_post+str(bs_id)
hist_name_85 = hist_name_pre+"85"+hist_name_post+str(bs_id)
hist_name_77 = hist_name_pre+"77"+hist_name_post+str(bs_id)
hist_name_70 = hist_name_pre+"70"+hist_name_post+str(bs_id)
hist_name_60 = hist_name_pre+"60"+hist_name_post+str(bs_id)

hist_std_100 = f_std.Get(folder_path+hist_name_truth)
hist_std_85 = f_std.Get(folder_path+hist_name_85)
hist_std_77 = f_std.Get(folder_path+hist_name_77)
hist_std_70 = f_std.Get(folder_path+hist_name_70)
hist_std_60 = f_std.Get(folder_path+hist_name_60)

print("hist_std_100.Integral() = {}".format(hist_std_100.Integral()))
print("hist_std_85.Integral() = {}".format(hist_std_85.Integral()))
print("hist_std_77.Integral() = {}".format(hist_std_77.Integral()))
print("hist_std_70.Integral() = {}".format(hist_std_70.Integral()))
print("hist_std_60.Integral() = {}".format(hist_std_60.Integral()))

hist_PCFT_100 = f_PCFT.Get(folder_path+hist_name_100)
hist_PCFT_85 = f_PCFT.Get(folder_path+hist_name_85)
hist_PCFT_77 = f_PCFT.Get(folder_path+hist_name_77)
hist_PCFT_70 = f_PCFT.Get(folder_path+hist_name_70)
hist_PCFT_60 = f_PCFT.Get(folder_path+hist_name_60)

int_60 = hist_PCFT_60.Integral()
int_70 = hist_PCFT_70.Integral()+int_60
int_77 = hist_PCFT_77.Integral()+int_70
int_85 = hist_PCFT_85.Integral()+int_77
int_100 = hist_PCFT_100.Integral()+int_85

print("hist_PCFT_100(reconstruct fixed cut).Integral() = {}".format(int_100))
print("hist_PCFT_85(reconstruct fixed cut).Integral() = {}".format(int_85))
print("hist_PCFT_77(reconstruct fixed cut).Integral() = {}".format(int_77))
print("hist_PCFT_70(reconstruct fixed cut).Integral() = {}".format(int_70))
print("hist_PCFT_60(reconstruct fixed cut).Integral() = {}".format(int_60))
