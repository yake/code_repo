# PCBT high-pt-extrapolation production

This is the steps for producing PCBT high-pt-extrapolation uncertainties for VRTrack jets. The steps should be easy to extend for other jet collections.

1. Produce the efficiency histograms by running the EfficiencyProducer in git repository:
https://gitlab.cern.ch/atlas-ftag-calibration/high-pT-extrapolation of tag "PFlow_ctags". Instructions on how to run it is written in README under this repository. Some modifications are needed for running a different jet collections in here. There is also a tag for running with release 22 in this repository.<br /><br />
In addition to the instruction under the repository, one need to setup certificate in order to submit jobs to condor by doing the following steps: <br />
`setupATLAS` <br />
`source setup.sh` <br />
`lsetup rucio`<br />
`voms-proxy-init -v2oms atlas` <br /><br />
The output will show: `Created proxy in /tmp/xxxxxxxxxxxx.` One need to copy this proxy to user's root directory `~/` and rename it as `.gridProxy`.

2. The produced Efficiency histograms can be hadded and stored in `combined` folder. These ouput are produced in FixedCut scheme. The PCBT scheme efficiency can be produced by running `prepare_PCFT_file.py`. The PCBT scheme histograms are produced by subtracting hisograms from two FixedCut working points. For example events passing 77%-85% is done by subtract events passing FixedCut 77% from events passing FixedCut 85%. Before running the scripts, input file names needed to be edited inside the script. Output file name will have postfix “_PCBT”.

3. We can produce CDI input text file with the PCBT efficiency root file produced from last step. The instruction is written in https://gitlab.cern.ch/atlas-ftag-calibration/high-pT-extrapolation/-/blob/master/CDITools/README.md. One thing to notice is that one need to modify the python script in order to produce 5 working point(0-60%, 60-70%, 70-77%, 77-85%, 85-100%) CDI input file. This can be done by editing the lines in:
https://gitlab.cern.ch/atlas-ftag-calibration/high-pT-extrapolation/-/blob/aa4e43773117a8ba4a44eb0882ffbc9b9e58b90e/CDITools/RunCDIInputProductionCampaign.py#L55
https://gitlab.cern.ch/atlas-ftag-calibration/high-pT-extrapolation/-/blob/aa4e43773117a8ba4a44eb0882ffbc9b9e58b90e/CDITools/plcdi_uncs.py#L76

In the mean time, plots will be made so that one can check the uncertainties.

3. Once the high-pT PCBT CDI input text file is produced. We can run the eigenvector decomposition on the cdi input text file and get the high-pT eigenvector uncertainties. This step is done by `produce_EV_CDI_input/produce_CDI_inputs.py` The eigenvector uncertainties are also pruned so that less high-pT uncertainties left in the output CDI input file. The number of EV uncertainties retained can be determined inside the script. The input CDI file should be placed inside `produce_EV_CDI_input/inputs` folder. Note that in this step, the extrapolation uncertainty in high-pT is subtract by reference pt bin uncertainty value.

4. Then combine the standard CDI input text file (from https://gitlab.cern.ch/atlas-ftag-calibration/comb_CDI) with the high-pt extrapolation EV input text file just produced from the previous step. To do this, put the standard input text file and EV input text file in ./mergeCDIinputs. Then run mergeCDIinputs.py to get the combined CDI input file.

5. This step is to produce the CDI file by using https://gitlab.cern.ch/atlas-ftag-calibration/comb_CDI. First replace the CDI input with the combine CDI input text file produced from previous step. Then produce PCBT follow the instruction in: https://gitlab.cern.ch/zfeng/mydocumentaions/-/blob/master/CDI-LocalBuild.md. Then produce the resulting full CDI following instructions in: https://gitlab.cern.ch/atlas-ftag-calibration/comb_CDI/-/blob/master/README.md


