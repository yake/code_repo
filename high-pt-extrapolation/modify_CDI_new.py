# Code based on Ilaria Luise's code
# Used for copy SF for B,C and light from one CDI file to another.
# This is because we want to copy high-pt-extrapolation result from one CDI file to another

import ROOT
import os
#import numpy as np

ROOT.xAOD.Init().ignore()
from ROOT import *


def remove_map(cdifilename): #,mapname):
        jetcol = 'AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903'
        tagger = 'DL1r'
        wp = 'Continuous'

        for flav in ['B','C','Light']:
                fileHandle = ROOT.TFile.Open(cdifilename,'update')
                taggerdir = fileHandle.Get(tagger)
                jetcoldir = taggerdir.Get(jetcol)
                wpdir = jetcoldir.Get(wp)
                flavdir = wpdir.Get(flav)

                flavdir.Delete('default_SF'+';*')
                #if flav == 'B':
                #        flavdir.Delete('default_SF'+';*')
                #elif flav == 'C':
                #        flavdir.Delete('ttbarC_SF'+';*')
                #elif flav == 'Light':
                #        flavdir.Delete('negative_tag_Zjet_SF'+';*')                
                fileHandle.Write()
                fileHandle.Close()

def add_map(cdifilename, infilename):
        jetCol = 'AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903'
        tagger = 'DL1r'
        wp = 'Continuous'

        for flav in ['B','C','Light']:
                mapname = 'default_SF'
                #if flav == 'B':
                #        mapname = 'default_SF'
                #elif flav == 'C':
                #        mapname ='ttbarC_SF'
                #elif flav == 'Light':
                #        mapname = 'negative_tag_Zjet_SF'
                
                # Reading in SF container from infile
                infile = ROOT.TFile.Open(infilename, 'read')
                ROOT.gDirectory.cd(tagger+'/'+jetCol+'/'+wp+'/'+flav+'/')
                SFcontainer = ROOT.Analysis.CalibrationDataHistogramContainer()
                ROOT.gDirectory.GetObject(mapname, SFcontainer)
                infile.Close()
                                
                # Writing out SF container to outfile
                outfile = ROOT.TFile.Open(cdifilename,'update')
                ROOT.gDirectory.cd(tagger+'/'+jetCol+'/'+wp+'/'+flav+'/')
                SFcontainer.Write(mapname,1)
                outfile.Close()
                
cdifilename = '2020-21-13TeV-MC16-CDI_smooth_VHLegacy-07Oct-v02.root'
infile = '2020-21-13TeV-MC16-CDI_smooth_extrap_as_EVs.root'
#modify_cutval(cdifilename)
remove_map(cdifilename)
add_map(cdifilename, infile)
