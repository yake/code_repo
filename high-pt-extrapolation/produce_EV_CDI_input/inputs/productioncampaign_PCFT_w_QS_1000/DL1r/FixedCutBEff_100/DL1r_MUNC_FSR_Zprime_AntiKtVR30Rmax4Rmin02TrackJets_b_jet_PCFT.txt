Analysis(Run2MCcalib,bottom,DL1r,FixedCutBEff_100,AntiKtVR30Rmax4Rmin02TrackJets){
	bin(20.0<pt<60.0,0<abseta<2.5)
	{
		central_value(1.00,0.003)
		sys(MC_FSR,8.65%)
	}
	bin(60.0<pt<100.0,0<abseta<2.5)
	{
		central_value(1.00,0.002)
		sys(MC_FSR,8.12%)
	}
	bin(100.0<pt<250.0,0<abseta<2.5)
	{
		central_value(1.00,0.001)
		sys(MC_FSR,2.98%)
	}
	bin(250.0<pt<400.0,0<abseta<2.5)
	{
		central_value(1.00,0.001)
		sys(MC_FSR,0.78%)
	}
	bin(400.0<pt<500.0,0<abseta<2.5)
	{
		central_value(1.00,0.001)
		sys(MC_FSR,0.17%)
	}
	bin(500.0<pt<700.0,0<abseta<2.5)
	{
		central_value(1.00,0.001)
		sys(MC_FSR,0.41%)
	}
	bin(700.0<pt<900.0,0<abseta<2.5)
	{
		central_value(1.00,0.002)
		sys(MC_FSR,1.01%)
	}
	bin(900.0<pt<1000.0,0<abseta<2.5)
	{
		central_value(1.00,0.002)
		sys(MC_FSR,1.41%)
	}
}
