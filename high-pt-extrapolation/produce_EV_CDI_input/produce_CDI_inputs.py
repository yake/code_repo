import ROOT
from collections import defaultdict

label_translator = {"b":"bottom", "c":"charm", "l":"light"}
tgw_translator = {"FixedCutBEff_60<tagweight":"60","FixedCutBEff_70<tagweight<FixedCutBEff_60":"70","FixedCutBEff_77<tagweight<FixedCutBEff_70":"77","FixedCutBEff_85<tagweight<FixedCutBEff_77":"85","tagweight<FixedCutBEff_85":"100"}

def format_dict(uncs_dict, axis):
    # ret_dict is in the form of ret_dict[pt][tagweight][Eigen_i]
    ret_dict = defaultdict(lambda: defaultdict(dict))
    # uncs_dict's key is Eigen_0, Eigen_1 ...
    pt_vec = []
    tgw_vec= []
    for key in uncs_dict:
        for bin in range(len(axis)):
            label = axis[bin]
            pt, tgw = label.split()
            ret_dict[pt][tgw][key] = uncs_dict[key][bin]
            if pt not in pt_vec:
                pt_vec.append(pt)
            if tgw not in tgw_vec:
                tgw_vec.append(tgw)
    return pt_vec, tgw_vec, ret_dict

def write_CDI_inputs(outdir, pt_vec, tgw_vec, uncs_dict, label, jet_collection):
    label_full = label_translator[label]
    outname_pre = "MCcalibCDI_DL1r_extrap_FixedCutBEff_"
    if label == "b":
        outname_post = "_"+jet_collection+"_retagging_"+label+"_jet_bhad_unc.txt"
    elif label == "c" or label == "l":
        outname_post = "_"+jet_collection+"_retagging_"+label+"_jet.txt"
    else:
        print("Not implemented!")
        exit()
    for tgw in tgw_vec:
        tgw_num = tgw_translator[tgw]
        fout = open(outdir+"FixedCutBEff_"+tgw_num+"/"+outname_pre+tgw_num+outname_post, "w")
        fout.write("Analysis(Run2MCcalib,"+label_full+",DL1r,FixedCutBEff_"+tgw_num+","+jet_collection+"){\n")
        for pt in pt_vec:
            fout.write("\tbin("+pt+",0<abseta<2.5)\n")
            fout.write("\t{\n")
            fout.write("\t\tcentral_value(1,00,0)\n")
            for ev_name in uncs_dict[pt][tgw]:
                unc = "{0:.2f}".format(uncs_dict[pt][tgw][ev_name]*100)
                fout.write("\t\tsys("+label+"_"+ev_name+","+unc+"%)\n")
            fout.write("\t}\n")
            fout.write("\n")
        fout.write("}")
        fout.close()

def produce_CDI_inputs(pruned_uncs_dict, axis, label):
    jet_collection = "AntiKtVR30Rmax4Rmin02TrackJets"
    
    outdir = "./output/"+label+"jets/"
    pt_vec, tgw_vec, formatted_uncs_dict = format_dict(pruned_uncs_dict, axis)
    write_CDI_inputs(outdir, pt_vec, tgw_vec, formatted_uncs_dict, label, jet_collection)

if __name__ == "main":
    main()
