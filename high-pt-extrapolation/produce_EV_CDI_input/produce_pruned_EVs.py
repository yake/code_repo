import ROOT
import numpy as np
import warnings
import math
import copy

import produce_CDI_inputs
warnings.filterwarnings("error")

# Read in systematic uncertainties

def hist_to_vec(hist):
    unc = []
    nbins = hist.GetNbinsX()
    for b in range(1, nbins+1):
        unc.append(hist.GetBinContent(b))
    return unc

def read_in_uncs(loc, path1, path2, path3, ref_bin, label, WPs, SFs):
    uncs_dict = {}
    for SF, WP in zip(SFs, WPs):
        uncs_dict = read_in_uncs_WP(loc+path1+WP+path2+WP+path3, ref_bin, uncs_dict, label, SF)
    return uncs_dict

def read_in_uncs_WP(loc, ref_bin, uncs_dict, label, SF):
    cur_uncs_dict = {}
    f1 = ROOT.TFile(loc+"modelling_important_extrap_uncs.root","READ")
    f2 = ROOT.TFile(loc+"tracking_detailed_1_extrap_uncs.root","READ")
    f3 = ROOT.TFile(loc+"tracking_detailed_2_extrap_uncs.root","READ")
    
    cur_uncs_dict["FSR"] = hist_to_vec(f1.Get("FSR"))
    cur_uncs_dict["parton shower"] = hist_to_vec(f1.Get("parton shower"))
    if label == "b":
        cur_uncs_dict["Quasi-stable"] = hist_to_vec(f1.Get("Quasi-stable"))

    cur_uncs_dict["TRK_FAKE_RATE_LOOSE_TIDE"] = hist_to_vec(f2.Get("TRK_FAKE_RATE_LOOSE_TIDE"))
    cur_uncs_dict["TRK_FAKE_RATE_LOOSE"] = hist_to_vec(f2.Get("TRK_FAKE_RATE_LOOSE"))
    cur_uncs_dict["TRK_EFF_LOOSE_TIDE"] = hist_to_vec(f2.Get("TRK_EFF_LOOSE_TIDE"))
    cur_uncs_dict["TRK_EFF_LOOSE_PP0"] = hist_to_vec(f2.Get("TRK_EFF_LOOSE_PP0"))
    cur_uncs_dict["TRK_EFF_LOOSE_PHYSMODEL"] = hist_to_vec(f2.Get("TRK_EFF_LOOSE_PHYSMODEL"))
    cur_uncs_dict["TRK_EFF_LOOSE_GLOBAL"] = hist_to_vec(f2.Get("TRK_EFF_LOOSE_GLOBAL"))
    cur_uncs_dict["TRK_EFF_LOOSE_IBL"] = hist_to_vec(f2.Get("TRK_EFF_LOOSE_IBL"))

    cur_uncs_dict["TRK_RES_D0_MEAS"] = hist_to_vec(f3.Get("TRK_RES_D0_MEAS"))
    cur_uncs_dict["TRK_RES_D0_DEAD"] = hist_to_vec(f3.Get("TRK_RES_D0_DEAD"))
    cur_uncs_dict["TRK_RES_Z0_MEAS"] = hist_to_vec(f3.Get("TRK_RES_Z0_MEAS"))
    cur_uncs_dict["TRK_RES_Z0_DEAD"] = hist_to_vec(f3.Get("TRK_RES_Z0_DEAD"))
    cur_uncs_dict["TRK_BIAS_QOVERP_SAGITTA_WM"] = hist_to_vec(f3.Get("TRK_BIAS_QOVERP_SAGITTA_WM"))
    cur_uncs_dict["TRK_BIAS_D0_WM"] = hist_to_vec(f3.Get("TRK_BIAS_D0_WM"))
    cur_uncs_dict["TRK_BIAS_Z0_WM"] = hist_to_vec(f3.Get("TRK_BIAS_Z0_WM"))

    # Delete all zeros in first ref_bin bins from unc vector.
    for key in cur_uncs_dict:
        # If reference bin is in the input, subtract pt ref bin value from all bins
        ##########################################
        #if ref_bin >= 0:
        #    nbins = len(cur_uncs_dict[key])
        #    ref_val = cur_uncs_dict[key][ref_bin]
        #    for bin in range(nbins):
        #        cur_uncs_dict[key][bin] -= ref_val
        ##########################################
        cur_uncs_dict[key] = cur_uncs_dict[key][ref_bin+1:]
        # Convert uncetainties from relative uncertianty to absolute uncertainty
        for i in range(len(cur_uncs_dict[key])):
            cur_uncs_dict[key][i] *= SF

    if len(uncs_dict) == 0:
        return cur_uncs_dict

    for key in uncs_dict:
        uncs_dict[key].extend(cur_uncs_dict[key])
    return uncs_dict

def get_cov_mat(uncs_dict):
    mat_order = len(list(uncs_dict.values())[0])
    cov_mat = np.zeros((mat_order,mat_order))
    for key in uncs_dict:
        x1 = np.array(uncs_dict[key])
        cov_mat += np.outer(x1,x1)
    return cov_mat

# Calculating denominator matrix of correlation matrix
def get_den_mat(uncs_dict):
    mat_order = len(list(uncs_dict.values())[0])
    variance_vec = np.zeros(mat_order)
    for key in uncs_dict:
        x1 = np.array(uncs_dict[key])
        variance_vec += np.multiply(x1, x1)
    den_mat = np.outer(np.sqrt(variance_vec), np.sqrt(variance_vec))
    return den_mat

# Calculate correlation matrix of one WP
# WP is determined by input directory "loc"
def get_corr_mat(uncs_dict):
    # Calculating covariance mat
    cov_mat = get_cov_mat(uncs_dict)
    # Calculating denominator mat
    den_mat = get_den_mat(uncs_dict)
    corr_mat = cov_mat/den_mat
    return corr_mat

def get_tmatrix(mat):
    mat_order = mat.shape[0]
    result = ROOT.TMatrixTSym(ROOT.double)(mat_order)
    for row in range(mat_order):
        for col in range(mat_order):
            result[row][col] = mat[row][col]
    return result

# Do eigen vector decomposition and return uncertainty vectors as dictionary
def get_ev_uncs(cov_mat):
    mat_order = cov_mat.shape[0]
    # Eigen vector decomposition
    cov_mat = get_tmatrix(cov_mat)
    eigenValueMaker = ROOT.TMatrixDSymEigen(cov_mat)
    eigen_vals = eigenValueMaker.GetEigenValues()
    eigen_vecs = eigenValueMaker.GetEigenVectors()
    eigen_vals = np.array([val for val in eigen_vals])
    eigen_np_vecs = [[] for i in range(mat_order)]
    for row in range(mat_order):
        for col in range(mat_order):
           eigen_np_vecs[row].append(eigen_vecs[row][col])
    eigen_vecs = eigen_np_vecs
    #eigen_vals, eigen_vecs = np.linalg.eig(cov_mat)

    # Reconstruct cov matrix
    #print(np.matmul(np.matmul(eigen_vecs, np.diag(eigen_vals)),eigen_vecs.T))

    # multiply eigenvalue with the corresponding eigenvector
    for idx in range(len(eigen_vals)):
        if abs(eigen_vals[idx]) < 1E-12:
            eigen_vals[idx] = abs(eigen_vals[idx])
    eigen_vals = np.sqrt(eigen_vals)
    ev_uncs = np.matmul(eigen_vecs, np.diag(eigen_vals))
    ev_uncs = ev_uncs.T

    ev_uncs_dict = {}
    for ev_id, ev_unc in enumerate(ev_uncs):
        ev_uncs_dict["Eigen_"+str(ev_id)] = ev_unc.tolist()
    return ev_uncs_dict

# convert EV uncertainties from absolute uncertainty to relative uncertainty
def get_rel_ev_uncs(ev_uncs_dict, SFs):
    nWP = len(SFs)
    for key in ev_uncs_dict:
        nbins = len(ev_uncs_dict[key])
        npT = nbins/nWP
        for WPbin in range(nWP):
            for pTbin in range(npT):
                bin = WPbin*npT + pTbin
                ev_uncs_dict[key] [bin]/= SFs[WPbin]
    return ev_uncs_dict

def get_axis(loc):
    f = ROOT.TFile(loc+"global_extrap_uncs.root","READ")
    hist = f.Get("total")
    nbins = hist.GetNbinsX()
    start_bin = 1
    axis = []
    for b in range(1, nbins+1):
        if hist.GetBinContent(b) > 1E-20:
            start_bin = b
            break
    WPs = ["FixedCutBEff_60<tagweight","FixedCutBEff_70<tagweight<FixedCutBEff_60","FixedCutBEff_77<tagweight<FixedCutBEff_70","FixedCutBEff_85<tagweight<FixedCutBEff_77","tagweight<FixedCutBEff_85"]
    for WP in WPs:
        for b in range(start_bin, nbins+1):
            pt_low = int(hist.GetXaxis().GetBinLowEdge(b))
            pt_up = int(hist.GetXaxis().GetBinLowEdge(b+1))
            bin_label = str(pt_low)+"<pt<"+str(pt_up)+" " + WP
            axis.append(bin_label)
        #axis.append(hist.GetXaxis().GetBinUpEdge(nbins))
    return axis

def get_TH2D_from_mat(mat, axis, name):
    nrows, ncols = mat.shape
    assert nrows == ncols
    nbins = len(axis)
    hist2d = ROOT.TH2D(name, name, nbins, 0, nbins, nbins, 0, nbins)
    for row in range(1, nrows+1):
        for col in range(1, ncols+1):
            b = hist2d.GetBin(row, col)
            hist2d.SetBinContent(b, mat[row-1,col-1])
    for b, label in enumerate(axis):
        hist2d.GetXaxis().SetBinLabel(b+1, label)
        hist2d.GetYaxis().SetBinLabel(b+1, label)
    hist2d.GetXaxis().SetTitle("tagweight & pt bin")
    hist2d.GetYaxis().SetTitle("tagweight & pt bin")
    return hist2d

def get_TH1D_from_vec(vec, axis, name):
    nbins = len(axis)
    hist1d = ROOT.TH1D(name, name, nbins, 0, nbins)
    for b in range(1, nbins+1):
        hist1d.SetBinContent(b, vec[b-1])
        hist1d.GetXaxis().SetBinLabel(b, axis[b-1])
    hist1d.GetXaxis().SetTitle("tagweight & pt bin")
    return hist1d

def draw_hist(hist, name):
    ROOT.gROOT.SetStyle("ATLAS")
    if hist.GetDimension() == 2:
        c = ROOT.TCanvas("c","c",800,800)
        c.cd()
        P_1 = ROOT.TPad("p1","p1",0,0,1,1)
        P_1.Draw()
        P_1.SetTopMargin(0.2)
        P_1.SetBottomMargin(0.2)
        P_1.SetRightMargin(0.2)
        P_1.SetLeftMargin(0.2)
        P_1.cd()
        hist.Draw("colz")
    elif hist.GetDimension() == 1:
        c = ROOT.TCanvas("c","c",2800,800)
        c.cd()
        P_1 = ROOT.TPad("p1","p1",0,0,1,1)
        P_1.Draw()
        P_1.SetTopMargin(0.05)
        P_1.SetBottomMargin(0.2)
        P_1.SetRightMargin(0.05)
        P_1.SetLeftMargin(0.05)
        P_1.cd()
        hist.Draw("hist")
    else:
        print("Error histogram dimension! Exiting...")
        exit()
    c.SaveAs(name + ".pdf")
    c.Close()

def draw_hist_2panel(h_nom, h_den, h_ratio, name):
    ROOT.gROOT.SetStyle("ATLAS")
    c = ROOT.TCanvas("c","c",2800,2000)
    c.cd()
    P_1 = ROOT.TPad("p1","p1",0,0.05,1,0.3)
    P_2 = ROOT.TPad("p2","p2",0,0.3,1,1)
    P_1.Draw()
    P_2.Draw()
    P_1.SetRightMargin(0.05)
    P_1.SetLeftMargin(0.05)
    P_1.SetTopMargin(0)
    P_1.SetBottomMargin(0.3)
    P_2.SetRightMargin(0.05)
    P_2.SetLeftMargin(0.05)
    P_2.SetBottomMargin(0)
    
    P_2.cd()
    h_den.SetLineColor(ROOT.kBlack)
    h_nom.SetLineColor(ROOT.kRed)
    h_den.Draw("hist")
    h_nom.Draw("hist same")
    L = ROOT.TLegend(0.694, 0.779, 0.886, 0.911)
    L.SetFillColor(0);
    L.SetLineWidth(0);
    L.SetTextSize(0.035);
    L.SetBorderSize(1);
    L.SetLineColor(ROOT.kWhite);
    L.AddEntry(h_den,"total","lep")
    L.AddEntry(h_nom,"pruned","lep")
    L.Draw()
    
    P_1.cd()
    h_ratio.Draw("hist")
    nbinsX = h_ratio.GetNbinsX()
    line = ROOT.TLine(0,0.99,nbinsX,0.99)
    line.SetLineColor(ROOT.kBlue)
    line.SetLineStyle(9)
    line.Draw();
    c.SaveAs(name + ".pdf")
    c.Close()

# Retain the best correlation by ranking uncertainties
# according to sum of gained ratio of each pT bin.
def prune_ev_uncs_best_corr(ev_uncs_dict, n_save):
    ev_uncs_dict_save = copy.deepcopy(ev_uncs_dict)
    retain_lst = []
    result = {}
    tot_unc = [0 for i in range(len(ev_uncs_dict["Eigen_0"]))]
    
    # Step1: First get quadrature sum uncertainties of all EVs
    full_unc = [0 for i in range(len(ev_uncs_dict["Eigen_0"]))]
    for idx in range(len(ev_uncs_dict)):
        ev_unc = ev_uncs_dict["Eigen_"+str(idx)]
        temp = []
        for v,u in zip(full_unc, ev_unc):
            temp.append(math.sqrt(v**2+u**2))
        full_unc = temp[:]
    
    # Step2: Ranking the uncertainties
    while len(retain_lst) < n_save:
        max_ratio = 0
        next_idx = ""
        next_tot = []
        for ev_idx in ev_uncs_dict:
            unc = ev_uncs_dict[ev_idx]
            # quadrature summing tot_unc with candidate unc
            temp_tot_unc = []
            for tot,can in zip(tot_unc, unc):
                temp_tot_unc.append(math.sqrt(tot**2+can**2))
            r_temp_tot_unc = 0
            for num,den in zip(temp_tot_unc, full_unc):
                r_temp_tot_unc += num/den
            if r_temp_tot_unc > max_ratio:
                max_ratio = r_temp_tot_unc 
                next_idx = ev_idx
                next_tot = temp_tot_unc
        tot_unc = next_tot
        retain_lst.append(next_idx)
        result[next_idx] = ev_uncs_dict[next_idx]
        del ev_uncs_dict[next_idx]
    
    # Restore ev_uncs_dict
    ev_uncs_dict = copy.deepcopy(ev_uncs_dict_save)
    return result

def prune_ev_uncs_normal(ev_uncs_dict, n_save):
    if n_save < 1:
        print("At least one uncertainty left after prune!")
    result = {}
    for idx in range(n_save):
        result["Eigen_"+str(idx)] = ev_uncs_dict["Eigen_"+str(idx)]
    return result

def get_qsum_uncs(uncs_dict):
    mat_order = len(list(uncs_dict.values())[0])
    variance_vec = np.zeros(mat_order)
    for key in uncs_dict:
        x1 = np.array(uncs_dict[key])
        variance_vec += np.multiply(x1, x1)
    return np.sqrt(np.sum(variance_vec))

def get_vec_qsum_uncs(uncs_dict):
    mat_order = len(list(uncs_dict.values())[0])
    variance_vec = np.zeros(mat_order)
    for key in uncs_dict:
        x1 = np.array(uncs_dict[key])
        variance_vec += np.multiply(x1, x1)
    return np.sqrt(variance_vec)

def get_combined_numerator(ev_uncs_dict):
    unc_vec = []
    mat_order = len(list(ev_uncs_dict.values())[0])
    for i in range(mat_order):
        unc = 0
        for key in ev_uncs_dict:
            ev_unc_vec = ev_uncs_dict[key]
            unc += ev_unc_vec[i]*np.sum(ev_unc_vec)
        unc_vec.append(unc)
    return unc_vec

def get_combined_denominator(ev_uncs_dict):
    unc_num_vec = get_combined_numerator(ev_uncs_dict)
    return np.sqrt(np.sum(unc_num_vec))

def combine_ev_uncs(ev_uncs_dict):
    num_vec = get_combined_numerator(ev_uncs_dict)
    den = get_combined_denominator(ev_uncs_dict)
    return [v/den for v in num_vec]

# jeff's pruning method which preserves correlation
def prune_ev_uncs_reduce(ev_uncs_dict, n_save):
    if n_save < 1:
        print("At least have one uncertainty after pruning!")
        return
    result_dict = {}
    for ev_id in range(n_save - 1):
        result_dict["Eigen_"+str(ev_id)] = ev_uncs_dict["Eigen_"+str(ev_id)]
        ev_uncs_dict.pop("Eigen_"+str(ev_id))
    result_dict["reduced"] = combine_ev_uncs(ev_uncs_dict)
    return result_dict    

def get_pruned_EVs(label, n_save):
    reduce_prune = False
    # WPs and SFs has to have same order
    SFs = [1.3581, 1.0609, 1.0097, 1.0157, 0.9763]
    WPs = ["60", "70", "77", "85", "100"]
    path1 = "FixedCutBEff_"
    path2 = "/plots_MCcalibCDI_DL1r_extrap_FixedCutBEff_"
    path3 = "_AntiKtVR30Rmax4Rmin02TrackJets_retagging_"+label+"_jet"
    if label == "b":
        input_folder = "./inputs/productioncampaign_PCFT_w_QS_1000/DL1r/" # bjets
        # pT reference bin index(0 indexed value!!!!!)
        # if ref_bin == -1, it means there is no ref bin. b:2 c: 3
        ref_bin = 2
        path3 += "_bhad_unc/"
    elif label == "c":
        input_folder = "./inputs/productioncampaign_PCFT_c/DL1r/" # cjets
        ref_bin = 3
        path3 += "/"
    elif label == "l":
        input_folder = "./inputs/productioncampaign_PCFT_l_mergebin_stopat_1200/DL1r/"
        ref_bin = 3
        path3 += "/"
    else:
        print("not implemented!")
        exit()
    uncs_dict = read_in_uncs(input_folder, path1, path2, path3, ref_bin, label, WPs, SFs)
    
    print("producing original correlation matrix")
    axis = get_axis(input_folder+path1+"60"+path2+"60"+path3)
    corr_mat = get_corr_mat(uncs_dict)
    full_hist2d = get_TH2D_from_mat(corr_mat, axis, "full")

    print("producing pruned correlation matrix")
    ev_uncs_dict = get_ev_uncs(get_cov_mat(uncs_dict))
    if reduce_prune:
        pruned_uncs_dict = prune_ev_uncs_reduce(ev_uncs_dict, n_save)
    else: # normal prune
        #pruned_uncs_dict = prune_ev_uncs_normal(ev_uncs_dict, n_save)
        pruned_uncs_dict = prune_ev_uncs_best_corr(ev_uncs_dict, n_save)
    pruned_corr_mat = get_corr_mat(pruned_uncs_dict)
    pruned_hist2d = get_TH2D_from_mat(pruned_corr_mat, axis, "pruned")
    
    # Making difference plot
    print("Drawing difference plot...")
    diff_hist2d = full_hist2d.Clone()
    diff_hist2d.Add(pruned_hist2d, -1)
    draw_hist(full_hist2d, "full_PCBT_"+label)
    
    # Making ratio plot
    diff_over_full_hist2d = diff_hist2d.Clone()
    diff_over_full_hist2d.Divide(full_hist2d)
    if reduce_prune:
        draw_hist(pruned_hist2d, "pruned_reduce_"+str(n_save)+"_nsave_PCBT_"+label)
        draw_hist(diff_hist2d, "diff_reduce_"+str(n_save)+"_nsave_PCBT_"+label)
        draw_hist(diff_over_full_hist2d, "ratio_reduce_"+str(n_save)+"_nsave_PCBT_"+label)
    else:
        draw_hist(pruned_hist2d, "pruned_normal_"+str(n_save)+"_nsave_PCBT_"+label)
        draw_hist(diff_hist2d, "diff_normal_"+str(n_save)+"_nsave_PCBT_"+label)
        draw_hist(diff_over_full_hist2d, "ratio_normal_"+str(n_save)+"_nsave_PCBT_"+label)
    full_qsum = get_qsum_uncs(uncs_dict)
    pruned_qsum = get_qsum_uncs(pruned_uncs_dict)
    print("uncs qsum ratio pruned/full = {}".format(pruned_qsum/full_qsum))

    full_qsum_vec = get_vec_qsum_uncs(uncs_dict)
    pruned_qsum_vec = get_vec_qsum_uncs(pruned_uncs_dict)
    ratio_vec = np.divide(pruned_qsum_vec, full_qsum_vec)

    h_full = get_TH1D_from_vec(full_qsum_vec, axis, "full_qsum_vec_"+label)
    h_pruned = get_TH1D_from_vec(pruned_qsum_vec, axis, "pruned_qsum_vec_"+str(n_save)+"_"+label)
    h_ratio = get_TH1D_from_vec(ratio_vec, axis, "pruned_full_ratio_vec_"+str(n_save)+"_"+label)
    draw_hist_2panel(h_pruned, h_full, h_ratio, "ratio_vec_"+str(n_save)+"_"+label)
        
    # draw histogram for remaining eigenvectors
    for key in pruned_uncs_dict:
        #ratio_vec = np.divide(ev_uncs_dict[key], full_qsum_vec)
        #hist1d = get_TH1D_from_vec(ratio_vec, axis, key)
        hist1d = get_TH1D_from_vec(pruned_uncs_dict[key], axis, key)
        draw_hist(hist1d, key+"_"+label)

    pruned_uncs_dict = get_rel_ev_uncs(pruned_uncs_dict, SFs)
    produce_CDI_inputs.produce_CDI_inputs(pruned_uncs_dict, axis, label)
    
if __name__ == "__main__":
    # get_pruned_EVs("flavour", "number of EV uncertainties you want to retain")
    get_pruned_EVs("b", 4)
    get_pruned_EVs("c", 2)
    get_pruned_EVs("l", 3)
    
