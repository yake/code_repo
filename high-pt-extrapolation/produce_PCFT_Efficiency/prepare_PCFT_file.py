import ROOT
import sys

# a helper function helps fetching BootStrap id
def get_bootstrap_id(histname):
    bs_id_str_index = histname.find("bootstrap_")
    bs_id_str_index += 10
    bs_id_str = histname[bs_id_str_index:]
    return int(bs_id_str)

def produce_PCFT_hists(taggedb_h_dict, fixed_cut_wp):
    n_bootstrap = len(taggedb_h_dict[fixed_cut_wp[0]])
    n_wp = len(fixed_cut_wp)
    for wp_id, wp in enumerate(fixed_cut_wp):
        # No need to do subtraction for lowest WP
        # So skip the last one
        if wp_id < n_wp - 1:
            next_wp = fixed_cut_wp[wp_id + 1]
            for bs_id in range(n_bootstrap):
                taggedb_h_dict[wp][bs_id].\
                    Add(taggedb_h_dict[next_wp][bs_id], -1)
    # Store result in a vector
    result_h_vec = []
    fixed_cut_wp.reverse()
    for wp in fixed_cut_wp:
        for bs_id in range(n_bootstrap):
            result_h_vec.append(taggedb_h_dict[wp][bs_id])
    
    print("succesfully produced PCFT")
    return result_h_vec

def get_beff_100_hist(hist, taggedb_name, \
                          taggedb_title, bs_id, fixed_cut_wp):
    # Rename name and title
    for wp in fixed_cut_wp:
        if (wp in taggedb_name) and (wp in taggedb_title):
            # Rename efficiency
            taggedb_name = taggedb_name.replace(wp, fixed_cut_wp[0])
            taggedb_title = taggedb_title.replace(wp, fixed_cut_wp[0])
            # Rename id
            bs_id_str_index = taggedb_name.find("bootstrap_")
            bs_id_str_index += 10
            taggedb_name = taggedb_name[:bs_id_str_index]+str(bs_id)
            taggedb_title = taggedb_title[:bs_id_str_index]+str(bs_id)
            break
    result_hist = hist.Clone(taggedb_name)
    result_hist.SetTitle(taggedb_title)
    return result_hist

def get_taggedb_name_title(work_dir):
    # Get taggedb hist name and title
    taggedb_name = ""
    taggedb_title = ""
    for key in work_dir.GetListOfKeys():
        hist = key.ReadObj()
        if "taggedb" in hist.GetName():
            taggedb_name = hist.GetName()
            taggedb_title = hist.GetTitle()
            break
    return taggedb_name, taggedb_title

def produce_PCFT_one_dir(work_dir):
    truthb_h_vec = []
    # taggedb_h_dict is 2d dictionary
    # for example taggedb_h_dict["FixedCutBEff_60"][3]
    # where 3 is bootstrap index
    taggedb_h_dict = {}
    # Working point need to be in decreasing order
    fixed_cut_wp = ["FixedCutBEff_100", "FixedCutBEff_85", "FixedCutBEff_77", \
                        "FixedCutBEff_70", "FixedCutBEff_60"]
    for wp in fixed_cut_wp:
        taggedb_h_dict[wp] = {}
    taggedb_name, taggedb_title = get_taggedb_name_title(work_dir)
    # Loading in histograms
    for key in work_dir.GetListOfKeys():
        hist = key.ReadObj()
        if "truthb" in hist.GetName():
            truthb_h_vec.append(hist)
            # Store it also as 100% WP taggedb
            bs_id = get_bootstrap_id(hist.GetName())
            hist_100 = get_beff_100_hist(hist, taggedb_name, \
                                             taggedb_title, bs_id,\
                                             fixed_cut_wp)
            taggedb_h_dict[fixed_cut_wp[0]][bs_id] = hist_100
        elif "taggedb" in hist.GetName():
            for wp in fixed_cut_wp:
                if wp in hist.GetName():
                    bs_id = get_bootstrap_id(hist.GetName())
                    taggedb_h_dict[wp][bs_id] = hist
        else:
            print("Not a valid hist name: {}".format(hist.GetName()))
            print("Exiting...")
            sys.exit()
    # Sanity check on the number of bootstrap for all WP
    n_bootstrap = len(taggedb_h_dict[fixed_cut_wp[0]])
    for wp in fixed_cut_wp:
        assert len(taggedb_h_dict[wp]) == n_bootstrap
    print("checked that there are {} (bootstrap replica + nom)".\
              format(n_bootstrap))
    PCBT_h_vec = produce_PCFT_hists(taggedb_h_dict, fixed_cut_wp)
    return truthb_h_vec + PCBT_h_vec
    
def produce_PCFT_dir(original_dir_key, f_out, folder_path):
    original_dir = original_dir_key.ReadObj()
    dir_keys = original_dir.GetListOfKeys()
    # In FSR file, there are 3 dirs under Nominal dir.
    for dir_key in dir_keys:
        work_dir = dir_key.ReadObj()
        h_vec = produce_PCFT_one_dir(work_dir)
        # Write histograms in output file
        current_folder = folder_path+"/"+dir_key.GetName()
        f_out.mkdir(current_folder)
        f_out.cd(current_folder)
        for hist in h_vec:
            hist.Write()
            
def produce_PCFT_file(input_file_name):
    f_in = ROOT.TFile(input_file_name+".root","READ")
    f_out = ROOT.TFile(input_file_name+"_PCFT.root","RECREATE")
    lv0_dir_name = "AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903"
    if len(f_in.GetListOfKeys()) != 1:
        print("Not exactly one folder under root directory. Exit...")
        sys.exit()
    for lv1_dir_key in f_in.GetListOfKeys():
    
        # Writing the same folder in output
        f_out.mkdir(lv1_dir_key.GetName())

        print("cd to directory:{}".format(lv1_dir_key.GetName()))
        lv1_dir = lv1_dir_key.ReadObj()
        print("Preparing PCFT for sub directory:")
        lv1_dir_keys = lv1_dir.GetListOfKeys()
        for lv2_dir_key in lv1_dir_keys:
            print(lv2_dir_key.GetName())
            # Writing in output with same file structure
            cur_folder_path = lv1_dir_key.GetName()+"/"+lv2_dir_key.GetName()
            f_out.mkdir(cur_folder_path)
            # Producing PCBT histograms and write to output
            produce_PCFT_dir(lv2_dir_key, f_out, cur_folder_path)

if __name__ == "__main__":
    files = ["DL1r_MUNC_FSR_Zprime_AntiKtVR30Rmax4Rmin02TrackJets_b_jet",
             "DL1r_MUNC_PS_Zprime_AntiKtVR30Rmax4Rmin02TrackJets_b_jet",
             "DL1r_TUNC_BLS_nrc_Zprime_AntiKtVR30Rmax4Rmin02TrackJets_b_jet",
             "DL1r_TUNC_BLS_rc_Zprime_AntiKtVR30Rmax4Rmin02TrackJets_b_jet",
             "DL1r_TUNC_NBLS_nrc_Zprime_AntiKtVR30Rmax4Rmin02TrackJets_b_jet",
             "DL1r_TUNC_NBLS_rc_Zprime_AntiKtVR30Rmax4Rmin02TrackJets_b_jet"]
    for std_file in files:
        print("processing file: {}".format(std_file+".root"))
        produce_PCFT_file(std_file)
