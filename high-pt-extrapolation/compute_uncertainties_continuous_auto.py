import ROOT
import re

def read_in_central_unc(f_in):
    pattern = ",(.*?)\)"
    central_unc_vec = []
    for line in f_in:
        if "central_value" in line:
            central_unc_str = re.search(pattern,line).group(1)
            central_unc_vec.append(central_unc_str)
    f_in.close()
    return central_unc_vec

def read_in_central_unc_files(WPs):
    central_unc_mat = []
    for WP in WPs:
        f_in = open("./normal_high_pt_CDI_input/FixedCutBEff_"+WP+"/DL1r_MUNC_FSR_Zprime_AntiKtVR30Rmax4Rmin02TrackJets_b_jet_PCFT.txt","r")
        central_unc_vec = read_in_central_unc(f_in)
        central_unc_mat.append(central_unc_vec)
    return central_unc_mat

def write_one_bin(f_out, unc, bin_low, bin_up, central_unc):
    f_out.write("\tbin("+str(bin_low)+"<pt<"+str(bin_up)+\
                    ",0<abseta<2.5)\n")
    f_out.write("\t{\n")
    f_out.write("\t\tcentral_value(1.00,"+central_unc+")\n")
    f_out.write("\t\tsys(metastable_interaction,{:.2f}%)\n".\
                    format(unc*100))
    f_out.write("\t}\n")    

def write_file(f_out, WP, unc_vec, binning, central_unc_vec):
    f_out.write("Analysis(Run2MCcalib,bottom,DL1r,FixedCutBEff_"+WP+",AntiKtVR30Rmax4Rmin02TrackJets){\n")
    for i in range(len(binning)-1):
        write_one_bin(f_out, unc_vec[i], \
                          binning[i], binning[i+1], central_unc_vec[i])
    f_out.write("}\n")
    f_out.close()

f_nom_60 = ROOT.TFile("./QS_histograms/QSPI_Eff_VR_All_1m_at60/Standard_DL1r_eff_All_at60.root","READ")
f_nom_70 = ROOT.TFile("./QS_histograms/QSPI_Eff_VR_All_1m_at70/Standard_DL1r_eff_All_at70.root","READ")
f_nom_77 = ROOT.TFile("./QS_histograms/QSPI_Eff_VR_All_1m_at77/Standard_DL1r_eff_All_at77.root","READ")
f_nom_85 = ROOT.TFile("./QS_histograms/QSPI_Eff_VR_All_1m_at85/Standard_DL1r_eff_All_at85.root","READ")

f_alt_60 = ROOT.TFile("./QS_histograms/QSPI_Eff_VR_All_1m_at60/QS_s3582_DL1r_eff_All_at60.root","READ")
f_alt_70 = ROOT.TFile("./QS_histograms/QSPI_Eff_VR_All_1m_at70/QS_s3582_DL1r_eff_All_at70.root","READ")
f_alt_77 = ROOT.TFile("./QS_histograms/QSPI_Eff_VR_All_1m_at77/QS_s3582_DL1r_eff_All_at77.root","READ")
f_alt_85 = ROOT.TFile("./QS_histograms/QSPI_Eff_VR_All_1m_at85/QS_s3582_DL1r_eff_All_at85.root","READ")

h_nom_num_60 = f_nom_60.Get("num_Eff_b__Eff__jet_bH_pt__DL1r_60")
h_nom_num_70 = f_nom_70.Get("num_Eff_b__Eff__jet_bH_pt__DL1r_70")
h_nom_num_77 = f_nom_77.Get("num_Eff_b__Eff__jet_bH_pt__DL1r_77")
h_nom_num_85 = f_nom_85.Get("num_Eff_b__Eff__jet_bH_pt__DL1r_85")
h_nom_num_100 = f_nom_85.Get("den_Eff_b__Eff__jet_bH_pt__DL1r_85")

h_alt_num_60 = f_alt_60.Get("num_Eff_b__Eff__jet_bH_pt__DL1r_60")
h_alt_num_70 = f_alt_70.Get("num_Eff_b__Eff__jet_bH_pt__DL1r_70")
h_alt_num_77 = f_alt_77.Get("num_Eff_b__Eff__jet_bH_pt__DL1r_77")
h_alt_num_85 = f_alt_85.Get("num_Eff_b__Eff__jet_bH_pt__DL1r_85")
h_alt_num_100 = f_alt_85.Get("den_Eff_b__Eff__jet_bH_pt__DL1r_85")

h_nom_num = []
h_nom_num.append(h_nom_num_60)
h_nom_num.append(h_nom_num_70)
h_nom_num.append(h_nom_num_77)
h_nom_num.append(h_nom_num_85)
h_nom_num.append(h_nom_num_100)

h_alt_num = []
h_alt_num.append(h_alt_num_60)
h_alt_num.append(h_alt_num_70)
h_alt_num.append(h_alt_num_77)
h_alt_num.append(h_alt_num_85)
h_alt_num.append(h_alt_num_100)

#binning = [20.0, 85.0, 140.0, 250.0, 400.0, 500.0, 600.0, 700.0, 800.0, 900.0, 1000.0, 1100.0, 1250.0, 1400.0, 1550.0, 1750.0, 2000.0, 2250.0, 2500.0, 2750.0, 3000.0]
#binning = [20.0, 60.0, 100.0, 250.0, 400.0, 500.0, 600.0, 700.0, 800.0, 900.0, 1000.0, 1100.0, 1250.0, 1400.0, 1550.0, 1750.0, 2000.0, 2250.0, 2500.0, 2750.0, 3000.0]
binning = [20., 60., 100., 250., 400., 500., 700., 900., 1100., 1400., 1700., 2000., 2500., 3000.]
central_unc_mat = []
WPs = ["60","70","77","85","100"]
h_nom_eff = []
h_alt_eff = []
h_eff_unc = []
h_eff_unc_mat = []

# Get all relative uncertainty histogram in
# h_eff_unc(histogram) and h_eff_unc_mat(vec of vec)
for i in range(len(WPs)):
    # continuous nominal efficiency
    h_nom_eff_cur = h_nom_num[i].Clone()
    if i == 0: 
        h_nom_eff_cur.Divide(h_nom_num[-1])
    else:
        h_nom_eff_cur.Add(h_nom_num[i-1],-1)
        h_nom_eff_cur.Divide(h_nom_num[-1])
    h_nom_eff.append(h_nom_eff_cur)

    # continuous alternative efficiency
    h_alt_eff_cur = h_alt_num[i].Clone()
    if i == 0: 
        h_alt_eff_cur.Divide(h_alt_num[-1])
    else:
        h_alt_eff_cur.Add(h_alt_num[i-1],-1)
        h_alt_eff_cur.Divide(h_alt_num[-1])
    h_alt_eff.append(h_alt_eff_cur)

    h_eff_unc_cur = h_alt_eff_cur.Clone()
    h_eff_unc_cur.Add(h_nom_eff_cur, -1)
    h_eff_unc_cur.Divide(h_nom_eff_cur)
    h_eff_unc.append(h_eff_unc_cur)

    unc_vec = []
    for i in range(h_eff_unc_cur.GetNbinsX()):
        unc_vec.append(h_eff_unc_cur.GetBinContent(i+1))
    h_eff_unc_mat.append(unc_vec)

central_unc_mat = read_in_central_unc_files(WPs)

# Write uncertainties into CDI input files
for idx,WP in enumerate(WPs):
    f_out = open("FixedCutBEff_"+WP+"_bjet.txt","w")    
    write_file(f_out, WP, h_eff_unc_mat[idx], binning, \
                   central_unc_mat[idx])

