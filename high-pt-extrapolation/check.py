import ROOT

f = ROOT.TFile("DL1r_TUNC_NBLS_rc_Zprime_AntiKtVR30Rmax4Rmin02TrackJets_b_jet.root","READ")
location_pre= "AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903/"

bs_id = "1"
unc_names = ["TRK_EFF_LOOSE_PP0", "TRK_EFF_LOOSE_PHYSMODEL","TRK_BIAS_QOVERP_SAGITTA_WM","TRK_BIAS_D0_WM","TRK_EFF_LOOSE_IBL","TRK_RES_Z0_DEAD","TRK_BIAS_Z0_WM","TRK_EFF_LOOSE_GLOBAL","TRK_FAKE_RATE_LOOSE_TIDE","TRK_FAKE_RATE_LOOSE","TRK_EFF_LOOSE_TIDE","TRK_RES_D0_DEAD"]

for unc_name in unc_names:
    h_alt_before_tag_0 = f.Get(location_pre + unc_name + "/nominal/truthb_nominal_bootstrap_" + bs_id)
    h_alt_after_tag_0 = f.Get(location_pre + unc_name + "/nominal/taggedb_FixedCutBEff_60|nominal_bootstrap_" + bs_id )
    
    h_nominal_before_tag_0 = f.Get(location_pre+"Nominal/nominal/truthb_nominal_bootstrap_"+bs_id )
    h_nominal_after_tag_0 = f.Get(location_pre+"Nominal/nominal/taggedb_FixedCutBEff_60|nominal_bootstrap_"+bs_id )

    print("")
    print("{} after tag".format(unc_name))
    for b in range(h_alt_after_tag_0.GetNbinsX()+1):
        print("{} , {}".format( \
          h_alt_after_tag_0.GetBinContent(b),\
          h_nominal_after_tag_0.GetBinContent(b)))
        if abs(h_alt_after_tag_0.GetBinContent(b)- h_nominal_after_tag_0.GetBinContent(b))<1E-7:
            print("Equal!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    
    
